#!/bin/sh
# Script to create the virtual environment if it doesn't exist already,
# install requirements, and enter the environment

VENV_DIR=venv

if [ ! -d $VENV_DIR ]
then
    python3 -m venv $VENV_DIR &&
    source $VENV_DIR/bin/activate &&
    pip3 install -r requirements.txt
fi

source $VENV_DIR/bin/activate
echo 'Virtual environment activated.'
