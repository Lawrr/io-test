from flask import request
from flask_socketio import emit
from app import socketio

class Client:
    def __init__(self, id, x, y, color):
        self.id = id
        self.x = x
        self.y = y
        self.color = color

    def to_serializable(self):
        return self.__dict__

clients = {}
speed = 32


@socketio.on('connect')
def handle_connect():
    players = [c.to_serializable() for c in clients.values()]
    emit('load_players', players)


@socketio.on('disconnect')
def handle_disconnect():
    del clients[request.sid]
    print("player left", request.sid)
    emit('player_left', request.sid, broadcast=True, include_self=False)


@socketio.on('spawn')
def handle_spawn(x, y, color):
    clients[request.sid] = Client(request.sid, x, y, color)
    emit('player_joined', (request.sid, x, y, color), broadcast=True, include_self=False)

@socketio.on('move')
def handle_move(dir):
    dx = 0
    dy = 0
    if dir == 0:  # Left
        dx = -speed
    elif dir == 1:  # Up
        dy = -speed
    elif dir == 2:  # Right
        dx = speed
    elif dir == 3:  # Down
        dy = speed

    c = clients[request.sid]
    new_x = c.x + dx
    new_y = c.y + dy

    new_x = min(800 - 32, max(0, new_x))
    new_y = min(800 - 32, max(0, new_y))

    c.x = new_x
    c.y = new_y
    emit('move', (request.sid, c.x, c.y), broadcast=True)

@socketio.on('ping')
def handle_ping():
    emit('pong', True)
